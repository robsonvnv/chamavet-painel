<?php

	class EmailMask {

		public function register() {

			return array(
				
				'type'		=> 'email',
				'label'		=> 'Email',
				'args'		=> array(

					'form-maker'	=> array('maxlength')

				)

			);

		}

		public function formMaker($data) {

			return '<input type="email" id="' . $data['col'] . '" name="' . $data['col'] . '" value="' . $data['default'] . '" class="form-control" required="' . $data['required'] . '" />';

		}

		public function paginator($data) {

			return $data;
			
		}


		public function filter($data) {

			return array(

				'condicao'	=> 'like',
				'prefix'	=> '%',
				'suffix'	=> '%',
				'value'		=> $data
			);

		}


		public function search() {

			return '<input type="text" id="value" name="value" class="form-control" />';

		}

	}

?>