<?php

	class PasswordMask {

		public function register() {

			return array(
				
				'type'		=> 'password',
				'label'		=> 'Senha',
				'args'		=> array(

					'form-maker' => array(
						
						'encrypt' => true

					)
				)

			);

		}


		public function formMaker($data) {

			if ($data['required'] == true) { $required = 'required'; } else { $required = ''; }
			return '<input type="password" id="' . $data['col'] . '" name="' . $data['col'] . '" class="form-control" ' . $required . ' />';

		}


		public function paginator($data) {

			return $data;

		}

		public function filter($data) {

			return array(

				'condicao'	=> '',
				'prefix'	=> '',
				'suffix'	=> '',
				'value'		=> $data

			);

		}


		public function search() {

			return '<input type="text" id="value" name="value" class="form-control" />';

		}

	}

?>