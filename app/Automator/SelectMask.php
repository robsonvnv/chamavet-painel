<?php

	class SelectMask {

		public function register() {

			return array(
				
				'type'		=> 'select',
				'label'		=> 'Select simples',
				'args'		=> array('options')

			);

		}

		public function formMaker($data) {

			$options	=	'<option value="">- Selecione -</option>';
			foreach ($data['options'] as $key => $value) {
				
				if ($data['default'] == $key) {

					$options .= '<option value="'. $key .'" selected>' . $value . '</option>';

				} else {
					
					$options .= '<option value="'. $key .'">' . $value . '</option>';

				}

			}

			return '
				
				<select id="' . $data['col'] . '" name="' . $data['col'] . '" class="form-control" required="' . $data['required'] . '">

					' . $options . '

				</select>

			';

		}

		public function paginator($data) {

			if ($data == '1') {

				return 'Ativo';

			} else {

				return 'Inativo';
				
			}
			
		}

		public function filter($data) {

			return array(

				'condicao'	=> '=',
				'prefix'	=> '',
				'suffix'	=> '',
				'value'		=> $data
			);

		}


		public function search() {

			return	'

				<select id="value" name="value" class="form-control">
					
					<option value="">- Selecione -</option>
					<option value="Ativo">Ativo</option>
					<option value="Inativo">Inativo</option>

				</select>
				
			';

		}

	}

?>