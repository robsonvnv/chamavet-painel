<?php

	class StatusMask {

		public function register() {

			return array(
				
				'type'		=> 'status',
				'label'		=> 'Select de status'

			);

		}

		public function formMaker($data) {

			if ($data['default'] == 1) {

				$ativo		= 'selected';
				$inativo	= '';

			} elseif($data['default'] == 0) {

				$ativo		= '';
				$inativo	= 'selected';

			} else {

				$ativo		= '';
				$inativo	= '';

			}

			return '
				
				<select name="' . $data['col'] . '" id="' . $data['col'] . '" class="form-control" required="' . $data['required'] . '">

					<option value="">- Selecione -</option>
					<option value="1" ' . $ativo . '>Ativo</option>
					<option value="0" ' . $inativo . '>Inativo</option>

				</select>

			';

		}

		public function paginator($data = null) {

			if ($data == '1') {

				return 'Ativo';

			} else {

				return 'Inativo';
				
			}
			
		}

		public function filter($data = null) {

			if ($data == 'Ativo') {

				$valor = 1;

			} else {

				$valor = 0;

			}

			return array(

				'condicao'	=> '=',
				'prefix'	=> '',
				'suffix'	=> '',
				'value'		=> $valor
			);

		}


		public function search() {

			return	'

				<select id="value" name="value" class="form-control">
					
					<option value="">- Selecione -</option>
					<option value="Ativo">Ativo</option>
					<option value="Inativo">Inativo</option>

				</select>
				
			';

		}

	}

?>