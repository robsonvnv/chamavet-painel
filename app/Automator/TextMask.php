<?php

	class TextMask {

		public function register() {

			return array(
				
				'type'		=> 'text',
				'label'		=> 'Texto Simples',
				'args'		=> array(
					
					'form-maker' => array('maxlenght')

				)

			);

		}


		public function formMaker($data) {


			return '<input type="text" id="' . $data['col'] . '" name="' . $data['col'] . '" value="' . $data['default'] . '" class="form-control" required="' . $data['required'] . '" />';

		}


		public function paginator($data) {

			return $data;

		}


		public function filter($data) {

			return array(

				'condicao'	=> 'like',
				'prefix'	=> '%',
				'suffix'	=> '%',
				'value'		=> $data
			);

		}


		public function search() {

			return '<input type="text" id="value" name="value" class="form-control" />';

		}

	}

?>