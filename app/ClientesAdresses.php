<?php

	namespace App;


	class ClientesAdresses {

		protected $table = 'users_adresses';
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'user_id', 'name'
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */

		public $timestamps = false;
		
	}
