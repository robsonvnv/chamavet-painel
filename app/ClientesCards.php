<?php

	namespace App;


	class ClientesCards {

		protected $table = 'users_cards';
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'user_id', 'name', 'bandeira', 'cvv'
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */

		public $timestamps = false;
		
	}
