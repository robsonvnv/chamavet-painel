<?php

	namespace App;


	class ClientesPets {

		protected $table = 'users_pets';
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'user_id', 'raca_id', 'name', 'age', 'gender'
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */

		public $timestamps = true;
		
	}
