<?php

	namespace App\Http\Controllers\Api;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Validator;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Response;
	use DB;

	class ClientesAdressesController extends Controller {
	

		public $content;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->content = array();

		}


		public function index(Request $request, $id = null) {

			$user = Auth::user();
			if ($user->status == 1) {

				if ($user->level == 1 || $user->level == 3) {

					$metodo	= $request->method();
					if ($metodo == 'GET') {

						return $this->show($user, $id);


					} elseif ($metodo == "POST") {
						
						return $this->create($user);

					} elseif ($metodo == "DELETE") {

						return $this->destroy($user, $id);

					}
				
				} else {

					$this->content['message'] = "Solicitação inválida.";
					$status = 401;
					return response()->json($this->content, $status);

				}

			} else {

				$this->content['message'] = "Solicitação inválida.";
				$status = 401;
				return response()->json($this->content, $status);

			}

		}


		public function show($user, $id) {

			$find	=	DB::table('users_adresses')
							->select('name', 'cep', 'estado', 'cidade', 'bairro', 'endereco', 'numero', 'complemento', 'coordenadas')
							->where('id', $id)
							->where('user_id', $user->id)
							->first();

			if (count($find) >= 1) {

				$this->content['data']	= $find;
				$status					= 200;

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu endereço não foi encontrado!";
				$status = 401;

			}

			return response()->json($this->content, $status);

		}


		public function create($user) {
			
			$validate	=	$this->validacao();
			if ($validate['result'] == true) {

				$this->content['message']	= $validate['message'];
				$status						= 400;

			} else {

				$values	= array(

					'user_id'		=> $user->id,
					'name'			=> request('name'),
					'cep'			=> request('cep'),
					'estado'		=> request('estado'),
					'cidade'		=> request('cidade'),
					'bairro'		=> request('bairro'),
					'endereco'		=> request('endereco'),
					'numero'		=> request('numero'),
					'complemento'	=> request('complemento'),
					'coordenadas'	=> request('coordenadas'),
					'created_at'	=> date('Y-m-d h:i:s')

				);

				$insert	= DB::table('users_adresses')->insert($values);
				if ($insert >= 1) {

					$this->content['message']	= "Endereço cadastrado com sucesso!";
					$status						= 200;

				} else {

					$this->content['message'] = "Ocorreu algum erro interno e seu endereço não pode ser cadastrado!";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}


		public function destroy($user, $id) {

			$excluir = DB::table('users_adresses')->where('id', $id)->where('user_id', $user->id)->delete();
			if($excluir >= 1) {

				$this->content['message']	= "Endereço excluido com sucesso!";
				$status						= 200;

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu endereço não pode ser excluido!";
				$status = 401;

			}

			return response()->json($this->content, $status);

		}


		public function validacao() {

			$error	= false;
			$fields	= array(

				'name'			=> 'Por favor digite um nome para este endereço',
				'cep'			=> 'Por favor digite o cep deste endereço',
				'estado'		=> 'Por favor selecione o estado deste endereço',
				'cidade'		=> 'Por favor digite a cidade deste endereço',
				'bairro'		=> 'Por favor digite o bairro deste endereço',
				'endereco'		=> 'Por favor digite o endereço',
				'numero'		=> 'Por favor digite o numero deste endereço',
				'coordenadas'	=> 'Ocorreu algum erro interno e seu endereço não pode ser cadastrado!'

			);

			foreach ($fields as $key => $value) {
				
				if ($error == false) {

					if (request($key) == '') {

						$retorno['message']	= $value;
						$error				= true;

					}

				}

			}

			$retorno['result'] = $error;
			return $retorno;

		}

	}

?>