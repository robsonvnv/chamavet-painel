<?php

	namespace App\Http\Controllers\Api;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Validator;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Response;
	use DB;

	class ClientesCardsController extends Controller {
	

		public $content;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->content = array();

		}


		public function index(Request $request, $id = null) {

			$user = Auth::user();
			if ($user->status == 1) {

				if ($user->level == 1 || $user->level == 3) {

					$metodo	= $request->method();
					if ($metodo == "GET") {

						return $this->listar($user->id);

					} elseif ($metodo == "POST") {
						
						return $this->create($user);

					} elseif ($metodo == "DELETE") {

						return $this->destroy($user, $id);

					}
				
				} else {

					$this->content['message'] = "Solicitação inválida.";
					$status = 401;
					return response()->json($this->content, $status);

				}

			} else {

				$this->content['message'] = "Solicitação inválida.";
				$status = 401;
				return response()->json($this->content, $status);

			}

		}

		
		public function listar($user) {

			$cards	= DB::table('users_cards')
						->select('name', 'bandeira', 'cvv')
						->where('user', $user)
						->get();

			$this->content['total']	=	count($cards);
			if(count($cards) >= 1) {

				$this->content['data'] = $cards;

			}

			$status = 200;
			return response()->json($this->content, $status);

		}


		public function create($user) {
			
			$validate	=	$this->validacao();
			if ($validate['result'] == true) {

				$this->content['message']	= $validate['message'];
				$status						= 400;

			} else {

				$values	= array(

					'user'			=> $user->id,
					'name'			=> request('name'),
					'bandeira'		=> request('bandeira'),
					'cvv'			=> request('cvv'),
					'created_at'	=> date('Y-m-d h:i:s')

				);

				$insert	= DB::table('users_cards')->insert($values);
				if ($insert >= 1) {

					$this->content['message']	= "Cartão cadastrado com sucesso!";
					$status						= 200;

				} else {

					$this->content['message'] = "Ocorreu algum erro interno e seu cartão não pode ser cadastrado!";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}


		public function destroy($user, $id) {

			$excluir = DB::table('users_cards')->where('id', $id)->where('user', $user->id)->delete();
			if($excluir >= 1) {

				$this->content['message']	= "Cartão excluido com sucesso!";
				$status						= 200;

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu cartão não pode ser excluido!";
				$status = 401;

			}

			return response()->json($this->content, $status);

		}


		public function validacao() {

			$error	= false;
			$fields	= array(

				'name'		=> 'Por favor digite o nome do seu cartão',
				'bandeira'	=> 'Por favor seleciona a bandeira do seu cartão',
				'cvv'		=> 'Por favor digite o código de segurança do seu cartão'

			);

			foreach ($fields as $key => $value) {
				
				if ($error == false) {

					if (request($key) == '') {

						$retorno['message']	= $value;
						$error				= true;

					}

				}

			}

			$retorno['result'] = $error;
			return $retorno;

		}

	}

?>