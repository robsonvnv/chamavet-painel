<?php

	namespace App\Http\Controllers\Api;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Validator;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Response;
	use DB;

	class ClientesChamadosController extends Controller {
	

		public $content;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->content = array();

		}


		public function index(Request $request, $id = null) {

			$user = Auth::user();
			if ($user->status == 1) {

				if ($user->level == 1 || $user->level == 3) {

					$metodo	= $request->method();
					if ($metodo == 'GET') {

						return $this->show($user, $id);


					} elseif ($metodo == "POST") {
						
						if (!$id || $id == null) {

							return $this->create($user);

						} else {
							
							return $this->cancel($user, $id);

						}

					}
				
				} else {

					$this->content['message'] = "Solicitação inválida.";
					$status = 401;
					return response()->json($this->content, $status);

				}

			} else {

				$this->content['message'] = "Solicitação inválida.";
				$status = 401;
				return response()->json($this->content, $status);

			}

		}


		public function show($user, $id) {

			$chamado	= $this->findChamado($id);
			if ($chamado['result'] == true) {

				$chamado	= $chamado['data'];
				$pet	= $this->findPet($user->id, $chamado->pet_id);
				if ($pet['result'] == true) {

					$pet		= $pet['data'];
					$address	= $this->findAddress($user->id, $chamado->endereco_id);
					if ($address['result'] == true) {

						$address	= $address['data'];
						$card		= $this->findCard($user->id, $chamado->card_id);
						if ($card['result'] == true) {

							$card		= $card['data'];
							$servico	= $this->findServico($chamado->servico_id);
							if ($servico['result'] == true) {

								$servico		= $servico['data'];
								$veterinario	= $this->findVeterinario($servico->user_id);
								if ($veterinario['result'] == true) {

									$veterinario	= $veterinario['data'];
									$values			= array(

										'pet'			=> $pet,
										'address'		=> $address,
										'card'			=> $card,
										'veterinario'	=> $veterinario,
										'servico'		=> array(

											'name'	=> $servico->name

										),
										'chamado'		=> array(

											'created'		=> $chamado->created_at,
											'observacoes'	=> $chamado->observacoes,
											'price'			=> $chamado->price,
											'status'		=> $chamado->status

										)

									);

									$this->content['result'] = $values;
									$status = 200;

								} else {

									$this->content['message'] = "Ocorreu algum erro interno e o veterinário vinculado a este chamado não foi encontrado!";
									$status = 401;

								}

							} else {

								$this->content['message'] = "Ocorreu algum erro interno e o serviço vinculado a este chamado não foi encontrado!";
								$status = 401;

							}

						} else {

							$this->content['message'] = "Ocorreu algum erro interno e seu cartão vinculado a este chamado não foi encontrado!";
							$status = 401;

						}

					} else {

						$this->content['message'] = "Ocorreu algum erro interno e seu endereço vinculado a este chamado não foi encontrado!";
						$status = 401;

					}

				} else {

					$this->content['message'] = "Ocorreu algum erro interno e seu pet vinculado a este chamado não foi encontrado!";
					$status = 401;

				}

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu chamado não foi encontrado!";
				$status = 401;

			}

			return response()->json($this->content, $status);

			
		}


		public function create($user) {
			
			$validate	=	$this->validacao();
			if ($validate['result'] == true) {

				$this->content['message']	= $validate['message'];
				$status						= 400;

			} else {

				$pet	= $this->findPet($user->id, request('pet_id'));
				if ($pet['result'] == true) {

					$address	= $this->findAddress($user->id, request('endereco_id'));
					if ($address['result'] == true) {

						$card	= $this->findCard($user->id, request('card_id'));
						if ($card['result'] == true) {

							$values	= array(

								'pet_id'		=> request('pet_id'),
								'endereco_id'	=> request('endereco_id'),
								'servico_id'	=> request('servico_id'),
								'card_id'		=> request('card_id'),
								'observacoes'	=> request('observacoes'),
								'created_at'	=> date('Y-m-d h:i:s')

							);

							$insert	= DB::table()->insert($values);
							if ($insert >= 1) {

								$this->content['message'] = "Seu chamado foi realizado com sucesso!";
								$status = 200;

							} else {

								$this->content['message'] = "Ocorreu algum erro interno e seu chamado não pode ser realizar!";
								$status = 401;

							}

						} else {
							
							$this->content['message'] = "Requisição inválida";
							$status = 401;

						}

					} else {

						$this->content['message'] = "Requisição inválida";
						$status = 401;

					}

				} else {

					$this->content['message'] = "Requisição inválida";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}


		public function cancel($user, $id) {

			$chamado	= $this->findChamado($id);
			if ($chamado['result'] == true) {

				$chamado	= $chamado['data'];
				$pet	= $this->findPet($user->id, $chamado->pet_id);
				if ($pet['result'] == true) {

					if ($chamado->status == 0) {

						$cancel	= DB::table('users_chamados')
									->where('id', $id)
									->update(array('status', '-1'));

						if ($cancel >= 1) {

							$this->content['message'] = "Seu chamado foi cancelado com sucesso!";
							$status = 200;

						} else {

							$this->content['message'] = "Ocorreu algum erro interno e seu chamado não pode ser cancelado!";
							$status = 401;

						}

					} elseif($chamado->status == 1) {
						
						$this->content['message'] = "Este chamado já foi aceito e não pode ser mais cancelado!";
						$status = 200;

					} else {

						$this->content['message'] = "Este chamado já está cancelado!";
						$status = 200;

					}

				} else {

					$this->content['message'] = "Ocorreu algum erro interno e seu chamado não foi encontrado!";
					$status = 401;

				}

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu chamado não foi encontrado!";
				$status = 401;

			}

			return response()->json($this->content, $status);

		}


		public function findPet($user, $pet) {

			$pet	=	DB::table('users_pets')
							->select('photo', 'name', 'gender', 'age')
							->where('user_id', $user)
							->where('id', $pet)
							->first();

			if (count($pet) >= 1) {

				$retorno['result']	= true;
				$retorno['data']	= $pet;

			} else {
				
				$retorno['result']	= false;

			}

			return $retorno;

		}


		public function findAddress($user, $address) {

			$address	=	DB::table('users_adresses')
								->select('cep', 'estado', 'cidade', 'bairro', 'endereco', 'numero', 'complemento')
								->where('user_id', $user)
								->where('id', $address)
								->first();

			if (count($address) >= 1) {

				$retorno['result']	= true;
				$retorno['data']	= $address;

			} else {
				
				$retorno['result']	= false;

			}

			return $retorno;

		}


		public function findCard($user, $card) {

			$card	=	DB::table('users_cards')
							->select('bandeira', 'name')
							->where('user_id', $user)
							->where('id', $card)
							->first();

			if (count($card) >= 1) {

				$retorno['result']	= true;
				$retorno['data']	= $card;

			} else {
				
				$retorno['result']	= false;

			}

			return $retorno;

		}


		public function findServico($servico) {

			$servico	=	DB::table('users_servicos')
								->select('user_id', 'servico_id')
								->where('id', $servico)
								->first();

			if (count($servico) >= 1) {

				
				$info	=	DB::table('servicos')
									->select('name', 'price')
									->where('id', $servico->servico_id)
									->where('status', 1)
									->first();

				if (count($info) >= 1) {

					$data				= array(

						'id'		=> $servico->servico_id,
						'user_id'	=> $servico->user_id,
						'name'		=> $info->name,
						'price'		=> $info->price

					);

					$retorno['result']	= true;
					$retorno['data']	= $data;

				} else {

					$retorno['result']	= false;

				}

			} else {
				
				$retorno['result']	= false;

			}

			return $retorno;

		}


		public function findVeterinario($veterinario) {

			$veterinario	=	DB::table('users')
									->select('name')
									->where('id', $veterinario)
									->where('level', 2)
									->where('status', 1)
									->first();

			if (count($veterinario) >= 1) {

				$retorno['result']	= true;
				$retorno['data']	= $veterinario;

			} else {
				
				$retorno['result']	= false;

			}

			return $retorno;

		}


		public function findChamado($id) {

			$chamado	= DB::table('users_chamados')
							->select('*')
							->where('id', $id)
							->first();

			if (count($chamado) >= 1) {

				$retorno['result']	= true;
				$retorno['data']	= $chamado;

			} else {
				
				$retorno['result']	= false;

			}

			return $retorno;

		}


		public function validacao() {

			$error	= false;
			$fields	= array(

				'pet_id'		=> 'Por favor selecione um de seus pets',
				'endereco_id'	=> 'Por favor selecione um de seus endereços',
				'servico_id'	=> 'Por favor selecione o serviço que deseja solicitar',
				'card_id'		=> 'Por favor selecione um de seus cartões de crédito para realizar o pagamento pelo serviço solicitado'

			);

			foreach ($fields as $key => $value) {
				
				if ($error == false) {

					if (request($key) == '') {

						$retorno['message']	= $value;
						$error				= true;

					}

				}

			}

			$retorno['result'] = $error;
			return $retorno;

		}

	}

?>