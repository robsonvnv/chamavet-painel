<?php

	namespace App\Http\Controllers\Api;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Validator;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Hash as Hash; 
	use App\User as User;
	use Response;
	use DB;

	class ClientesController extends Controller {
	

		public $content;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->content = array();

		}


		// Cadastro
		public function register() {

			$rules = array(
				
				'name'			=> 'required',
				'email'			=> 'required|email|unique:users,email',
				'gender'		=> 'required',
				'phone'			=> 'required',
				'password'		=> 'required',
				'platform'		=> 'required',
				'tokenFCM'		=> 'required'
			
			);

			$model = new User;

			$niceNames = [
				'name'		=> 'nome',
				'email'		=> 'e-mail',
				'gender'	=> 'sexo',
				'phone'		=> 'telefone',
				'password'	=> 'senha'
			]; 

			$messageDataFCM = 'Seu dispositivo não conseguiu enviar alguns dados, por favor reinicie ele e tente o cadastro novamente.';
			$messagesErrors = [

				'platform.required'	=> $messageDataFCM,
				'tokenFCM.required'	=> $messageDataFCM

			];

			$validator = Validator::make(Input::all(), $rules, $messagesErrors, $niceNames);

			if ($validator->fails()) {

				// get the error messages from the validator
				$messages = $validator->messages();
				$this->content['message'] = $validator->errors()->first();
				$status = 400;            

			} else {

				$model->platform		= request('platform');
				$model->FCMToken		= request('tokenFCM');
				$model->level			= 3;
				$model->name			= request('name');
				$model->email			= request('email');
				$model->password		= Hash::make(request('password'));
				$model->phone			= request('phone');
				$model->gender			= request('gender');
				$model->save();

				if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

					$user = Auth::user();
					$this->content['token']	= $user->createToken('Chamavet')->accessToken;
					$this->content['name']	= request('name');
					$this->content['email']	= request('email');

					$this->content['message'] = "Seu usuário foi criado com sucesso.";
					$status = 200;
					
				} else {

					$this->content['message'] = "Seu usuário foi criado com sucesso, porem seu login não pode ser realizado.";
					$status = 200;

				}

			}

			return response()->json($this->content, $status);

		}


		// Login com facebook
		public function facebook(){

			$rules = array(
				
				'email'			=> 'required',            
				'facebook'		=> 'required',	
				'typePlatform'	=> 'required',
				'tokenFCM'		=> 'required'
			
			);


			$niceNames = [

				'email'		=> 'e-mail',
				'facebook'	=> 'facebook'

			];


			$messageDataFCM = 'Seu dispositivo não conseguiu enviar alguns dados, por favor reinicie o aplicativo e tente o cadastro novamente.';
			$messagesErrors = [

				'typePlatform.required'	=> $messageDataFCM,
				'tokenFCM.required'		=> $messageDataFCM

			];

			$validator = Validator::make(Input::all(), $rules, $messagesErrors, $niceNames);

			if ($validator->fails()) {

				$messages = $validator->messages();

				$this->content['message'] = $validator->errors()->first();
				$status = 400;

			} else {
				
				$userData	= DB::table('users_data')->select('user')->where('facebook', request('facebook'))->first();
				$usuario = User::where('id', $userData->user)->where('email', request('email'))->first();
				if(count($usuario) >= 1) {

					if ($usuario->level == 1 || $usuario->level == 3) {

						Auth::loginUsingId($usuario->id);

						$user = Auth::user();

						DB::table('oauth_access_tokens')
							->where('user_id', '=', $user->id)
							->update(['revoked' => true]);

						DB::table('users')
							->where('id', '=', $user->id)
							->update([

								'platform'	=> request('typePlatform'),
								'FCMToken'	=> request('tokenFCM')

							]);

						$this->content['token'] =  $user->createToken('Chamavet')->accessToken;
						$this->content['name'] = $user->name;
						$this->content['email'] = $user->email;
						$status = 200;

					} else {

						$this->content['message'] = "Dados de acesso incorretos.";
						$status = 401;

					}

				} else {

					$this->content['message'] = "Dados de acesso incorretos.";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}



		// Login sem facebook
		public function login(){

			$rules = array(
				
				'email'			=> 'required',            
				'password'		=> 'required',	
				'typePlatform'	=> 'required',
				'tokenFCM'		=> 'required'
			
			);


			$niceNames = [

				'email'		=> 'e-mail',
				'password'	=> 'senha'

			];


			$messageDataFCM = 'Seu dispositivo não conseguiu enviar alguns dados, por favor reinicie o aplicativo e tente o cadastro novamente.';
			$messagesErrors = [

				'typePlatform.required'	=> $messageDataFCM,
				'tokenFCM.required'		=> $messageDataFCM

			];

			$validator = Validator::make(Input::all(), $rules, $messagesErrors, $niceNames);

			if ($validator->fails()) {

				$messages = $validator->messages();

				$this->content['message'] = $validator->errors()->first();
				$status = 400;

			} else {
				
				if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
				

					$user = Auth::user();
					if($user->status == 0){

						$this->content['message'] = "Seu usuário ainda não está ativo.";
						$status = 401;

					} else {

						if($user->level == 3 || $user->level == 1) {

							DB::table('oauth_access_tokens')
								->where('user_id', '=', $user->id)
								->update(['revoked' => true]);

							DB::table('users')
								->where('id', '=', $user->id)
								->update([

									'platform'	=> request('typePlatform'),
									'FCMToken'	=> request('tokenFCM')

								]);

							$this->content['token'] =  $user->createToken('Chamavet')->accessToken;
							$this->content['name'] = $user->name;
							$this->content['email'] = $user->email;
							$status = 200;

						} else {

							$this->content['message'] = "Dados de acesso incorretos.";
							$status = 401;

						}

					}

				} else {

					$this->content['message'] = "Dados de acesso incorretos.";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}

	}

?>