<?php

	namespace App\Http\Controllers\Api;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Validator;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Response;
	use DB;

	class ClientesPetsController extends Controller {
	

		public $content;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->content = array();

		}


		public function index(Request $request, $id = null) {

			$user = Auth::user();
			if ($user->status == 1) {

				if ($user->level == 1 || $user->level == 3) {

					$metodo	= $request->method();
					if ($metodo == 'GET') {

						return $this->show($user, $id);


					} elseif ($metodo == "POST") {
						
						if (!$id || $id == null) {

							return $this->create($user);

						} else {
							
							return $this->update($user, $id);

						}

					} elseif ($metodo == "DELETE") {

						return $this->destroy($user, $id);

					}
				
				} else {

					$this->content['message'] = "Solicitação inválida.";
					$status = 401;
					return response()->json($this->content, $status);

				}

			} else {

				$this->content['message'] = "Solicitação inválida.";
				$status = 401;
				return response()->json($this->content, $status);

			}

		}


		public function show($user, $id) {

			$find	=	DB::table('users_pets')
							->select('raca', 'photo', 'name', 'gender', 'age')
							->where('id', $id)
							->where('user_id', $user->id)
							->first();

			if (count($find) >= 1) {

				$this->content['data']	= $find;
				$status					= 200;

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu pet não foi encontrado!";
				$status = 401;

			}

			return response()->json($this->content, $status);

		}


		public function create($user) {
			
			$validate	=	$this->validacao();
			if ($validate['result'] == true) {

				$this->content['message']	= $validate['message'];
				$status						= 400;

			} else {

				$values	= array(

					'user'			=> $user->id,
					'raca'			=> request('raca_id'),
					'name'			=> request('name'),
					'age'			=> request('age'),
					'gender'		=> request('gender'),
					'created_at'	=> date('Y-m-d h:i:s')

				);

				$insert	= DB::table('users_pets')->insert($values);
				if ($insert >= 1) {

					$this->content['message']	= "Pet cadastrado com sucesso!";
					$status						= 200;

				} else {

					$this->content['message'] = "Ocorreu algum erro interno e seu pet não pode ser cadastrado!";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}


		public function update($user, $id) {

			$validate	=	$this->validacao();
			if ($validate['result'] == true) {

				$this->content['message']	= $validate['message'];
				$status						= 400;

			} else {

				$values	= array(

					'name'			=> request('name'),
					'age'			=> request('age'),
					'gender'		=> request('gender'),
					'updated_at'	=> date('Y-m-d h:i:s')

				);

				$update	= DB::table('users_pets')
							->where('id', $id)
							->where('user_id', $user->id)
							->update($values);

				if ($update >= 1) {

					$this->content['message']	= "Pet editado com sucesso!";
					$status						= 200;

				} else {

					$this->content['message'] = "Ocorreu algum erro interno e seu pet não pode ser editado!";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}


		public function destroy($user, $id) {

			$excluir = DB::table('users_pets')->where('id', $id)->where('user_id', $user->id)->delete();
			if($excluir >= 1) {

				$this->content['message']	= "Pet excluido com sucesso!";
				$status						= 200;

			} else {

				$this->content['message'] = "Ocorreu algum erro interno e seu pet não pode ser excluido!";
				$status = 401;

			}

			return response()->json($this->content, $status);

		}


		public function validacao() {

			$error	= false;
			$fields	= array(

				'raca'		=> 'Por favor selecione a raça do seu pet',
				'name'		=> 'Por favor digite o nome do seu pet',
				'age'		=> 'Por favor digite a idade do seu pet',
				'gender'	=> 'Por favor selecione o sexo do seu pet'

			);

			foreach ($fields as $key => $value) {
				
				if ($error == false) {

					if (request($key) == '') {

						$retorno['message']	= $value;
						$error				= true;

					}

				}

			}

			$retorno['result'] = $error;
			return $retorno;

		}

	}

?>