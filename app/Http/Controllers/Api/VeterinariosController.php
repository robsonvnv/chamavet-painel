<?php

	namespace App\Http\Controllers\Api;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Validator;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Hash as Hash; 
	use App\User as User;
	use Response;
	use DB;

	class VeterinariosController extends Controller {
	

		public $content;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->content = array();

		}


		public function status() {

			$user = Auth::user();
			if ($user->status == 1) {
				
				if ($user->level == 1 || $user->level == 2) {

					if ($user->enabled == 1) {

						$novo = 0;

					} else {
						
						$novo = 1;

					}

					$update	= DB::table('users')->where('id', $user->id)->update(array('enabled' => $novo));
					if ($update >= 1) {
						
						$this->content['message'] = "Você não está autorizado a acessar esta área.";
						$status = 200;

					} else {

						$this->content['message'] = "Ocorreu algum erro interno e seu status não pode ser alterado!";
						$status = 401;

					}

				} else {

					$this->content['message'] = "Você não está autorizado a acessar esta área.";
					$status = 400;

				}

			} else {

				$this->content['message'] = "Seu usuário não está autorizado a utilizar nosso sistema.";
				$status = 401;

			}
			
			return response()->json($this->content, $status);
			
		}


		// Login
		public function login(){

			$rules = array(
				
				'email'			=> 'required',            
				'password'		=> 'required',	
				'typePlatform'	=> 'required',
				'tokenFCM'		=> 'required'
			
			);


			$niceNames = [

				'email'		=> 'e-mail',
				'password'	=> 'senha'

			];


			$messageDataFCM = 'Seu dispositivo não conseguiu enviar alguns dados, por favor reinicie o aplicativo e tente o cadastro novamente.';
			$messagesErrors = [

				'typePlatform.required'	=> $messageDataFCM,
				'tokenFCM.required'		=> $messageDataFCM

			];

			$validator = Validator::make(Input::all(), $rules, $messagesErrors, $niceNames);

			if ($validator->fails()) {

				$messages = $validator->messages();

				$this->content['message'] = $validator->errors()->first();
				$status = 400;

			} else {
				
				if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
				

					$user = Auth::user();
					if($user->status == 0){

						$this->content['message'] = "Seu usuário ainda não está ativo.";
						$status = 401;

					} else {

						if($user->level == 2 || $user->level == 1) {

							DB::table('oauth_access_tokens')
								->where('user_id', '=', $user->id)
								->update(['revoked' => true]);

							DB::table('users')
								->where('id', '=', $user->id)
								->update([

									'platform'	=> request('typePlatform'),
									'FCMToken'	=> request('tokenFCM')

								]);

							$this->content['token'] =  $user->createToken('Chamavet')->accessToken;
							$this->content['name'] = $user->name;
							$this->content['email'] = $user->email;
							$status = 200;

						} else {

							$this->content['message'] = "Dados de acesso incorretos.";
							$status = 401;

						}

					}

				} else {

					$this->content['message'] = "Dados de acesso incorretos.";
					$status = 401;

				}

			}

			return response()->json($this->content, $status);

		}

	}

?>