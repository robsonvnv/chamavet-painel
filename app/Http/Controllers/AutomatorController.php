<?php
	
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Providers\AutomatorServiceProvider as Automator;
	use Response;
	use DB;


	class AutomatorController extends Controller {

		private $tables	= array('migrations', 'oauth_access_tokens', 'oauth_auth_codes', 'oauth_clients', 'oauth_personal_access_clients', 'oauth_refresh_tokens', 'password_resets');

		public function __construct() {

		}


		private function getTables() {

			$tables = DB::select('SHOW TABLES');
			foreach ($tables as $key => $value) {

				$valor		= $value->Tables_in_rm_chamavet_admin;
				if (!in_array($valor, $this->tables)) {
					
					$retorno[] = $valor;

				}

			}

			return $retorno;

		}


		public function cols() {

			$table	= request('table');
			if ((isset($table)) && (isset($table) != '')) {

				if (!in_array($table, $this->tables)) {

					$columns			= DB::select('SHOW COLUMNS FROM ' . $table);
					foreach ($columns as $col) {
						
						if ($col->Key != "PRI") {

							$cols[] = $col->Field;

						}

					}

					$retorno['cols']	= $cols;
					$retorno['result']	= true;

				} else {

					$retorno['result']	= false;

				}

			} else {

				$retorno['result']	= false;

			}

			// $retorno['table']	=	$table;
			return response()->json($retorno);
		}


		public function mask() {

			$tipo	= request('tipo');
			$funcao	= request('funcao');

			if ($tipo != '' && $funcao != '') {

				$mask = Automator::mask($tipo, $funcao);
				if ($mask['result'] == true) {

					return $mask['mask'];

				} else {

					return $mask['message'];
					
				}

			} else {

			}

		}


		private function getMasks() {

			$mascaras	=	scandir(base_path('app/Automator'));
			foreach ($mascaras as $key => $value) {
				
				if (strstr($value, 'Mask.php')) {

					$arquivo	= (base_path('app/Automator/') . $value);
					$classe		= str_replace('.php', '', $value);
					if (file_exists($arquivo)) {
						
						require_once($arquivo);
						$class	= new $classe();

						$mask		= $class->register();
						$masks[$mask['type']]	= $mask['label'];

					}

				}

			}

			return $masks;

		}


		public function index() {

			$args['tables']	=	$this->getTables();
			$args['masks']	=	$this->getMasks();

			echo '<pre>';
			print_r($args);
			echo '</pre>';

			return view('automator', $args);
		}

	}

?>