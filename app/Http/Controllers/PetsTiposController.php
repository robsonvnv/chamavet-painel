<?php
	
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Providers\AutomatorServiceProvider as Automator;
	use DB;


	class PetsTiposController extends Controller {
		
		private	$args;
		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */

		public function __construct() {

			$this->args	=	array(

				'page'	=>	array(

					'name'		=> 'Pets',
					'base'		=> 'pets/tipos/',
					'table'		=> 'pets_tipos',
					'redirect'	=> '5000',
					'list'		=> array(

						'enabled'	=> true,
						'title'		=> 'Listar tipos de pets',
						'search'	=> true,
						'results'	=> 15

					),
					'create'	=> array(

						'enabled'	=> true,
						'action'	=> 'store',
						'title'		=> 'Cadastrar tipo de pet',
						'messages'	=> array(

							'success'	=> 'Tipo de pet cadastrado com sucesso!',
							'danger'	=> 'O tipo de pet não pode ser cadastrado, tente novamente mais tarde'

						)

					),
					'update'	=> array(

						'enabled'	=> true,
						'action'	=> 'save',
						'messages'	=> array(

							'notfound'	=> 'O tipo de pet que você está tentando editar não foi encontrado!',
							'success'	=> 'Tipo de pet editado com sucesso!',
							'danger'	=> 'O tipo de pet não pode ser editado, tente novamente mais tarde'

						)

					),
					'destroy'	=> array(

						'enabled'	=> true,
						'title'		=> 'Excluir tipo de pet',
						'messages'	=> array(

							'single'	=> array(

								'success'	=> 'O tipo de pet foi excluido com sucesso!',
								'danger'	=> 'O tipo de pet não pode ser excluido!',
								'notfound'	=> 'O tipo de pet que você está tentando excluir não foi encontrado!'

							),
							'multiple'	=> array(

								'success'	=> 'Todos os tipos de pets selecionados foram excluidos com sucesso!',
								'parse'		=> 'Um ou mais tipos de pet não puderam ser excluidos!',
								'danger'	=> 'Os tipos de pet selecionados não puderam ser excluidos!',
								'empty'		=> 'Por favor selecione ao menos um tipo de pet para ser excluido!'

							)

						)

					),
					'cols'	=> array(

						'name'	=>	array(

							'type'		=> 'text',
							'paginator'	=> array(

								'enabled'	=> true,
								'label'		=> 'Nome',
								'search'	=> true,
								'order'		=> true

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'message'		=> 'Por favor preencha o campo Nome',
								'label'			=> 'Nome',
								'container'		=> '6',
								'clear'			=> false,
								'required'		=> true,
								'maxlength'		=> 30

							)

						),
						'status'	=>	array(

							'type'		=> 'status',
							'paginator'	=> array(

								'enabled'	=> true,
								'label'		=> 'Status',
								'search'	=> true,
								'order'		=> true

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'label'			=> 'Status',
								'message'		=> 'Por favor Selecione uma opção para o campo Status',
								'container'		=> '6',
								'clear'			=> true,
								'required'		=> true

							)

						)

					)

				)

			);

		}


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		 */


		public function index(Request $request) {

			$this->args['page']['title']		= 'Listar tipos de pet';

			$paginator							= array(

				'table'		=> $this->args['page']['table'],
				'cols'		=> $this->args['page']['cols'],
				'results'	=> $this->args['page']['list']['results'],
				'filters'	=> $request->all()

			);

			$this->args['page']['paginator']	= Automator::paginator($paginator);


			return view('layouts/paginator', $this->args);

		}


		public function create() {

			$this->args['page']['title']		=	'Cadastrar tipo de pet';

			$formMaker							= array(

				'table'		=> $this->args['page']['table'],
				'cols'		=> $this->args['page']['cols'],
				'action'	=> $this->args['page']['create']['action'],
				'id'		=> 0

			);

			$this->args['page']['form-maker']	= Automator::formMaker($formMaker);
			$this->args['page']['create']		=	false;
			$this->args['page']['destroy']		=	false;

			return view('layouts/form-maker', $this->args);

		}


		public function store(Request $request) {

			$this->args['page']['title'] = 'Cadastrar tipo de pet';
			$fields	= array(

				'name'			=>	$request->input('name'),
				'status'		=>	$request->input('status'),
				'created_at'	=>	date('Y-m-d h:i:s')

			);

			$error = '';
			foreach ($fields as $key => $value) {
				
				if ($error == '') {

					if ($value == '') {

						$error	= $this->args['page']['cols'][$key]['form-maker']['message'];

					}

				}

			}

			if ($error == '') {

				$insert	=	DB::table($this->args['page']['table'])->insert($fields);
				if ($insert >= 1) {

					$result	= 'success';

				} else {

					$result	= 'danger';

				}
				
				$retorno['message']	= $this->args['page']['create']['messages'][$result];


			} else {
				
				$result				= 'warning';
				$retorno['message']	= $error;
				
			}

			$retorno['result']				= $result;
			$this->args['page']['retorno']	= $retorno;
			
			return view('layouts/returns', $this->args);

		}


		public function update($id) {

			$this->args['page']['title'] = 'Editar tipo de pet';

			$formMaker							= array(

				'table'		=> $this->args['page']['table'],
				'cols'		=> $this->args['page']['cols'],
				'action'	=> $this->args['page']['update']['action'],
				'id'		=> $id

			);

			$this->args['page']['form-maker']	= Automator::formMaker($formMaker);

			return view('layouts/form-maker', $this->args);

		}


		public function save(Request $request) {

			$this->args['page']['title'] = 'Editar tipo de pet';
			$id		= $request->input('id');
			if ($id) {

				$fields	= array(

					'name'			=>	$request->input('name'),
					'status'		=>	$request->input('status'),
					'updated_at'	=>	date('Y-m-d h:i:s')

				);

				$error = '';

				foreach ($fields as $key => $value) {
						
					if ($error == '') {

						if ($value == '') {

							$error	= $this->args['page']['cols'][$key]['form-maker']['message'];

						}

					}

				}


				if ($error == '') {

					$update	= DB::table($this->args['page']['table'])->where('id', $id)->update($fields);
					if ($update >= 1) {

						$result = 'success';

					} else {

						$result = 'danger';

					}

					$message	= $this->args['page']['update']['messages'][$result];

				} else {

					$result		= 'warning';
					$message	= $error;

				}

			} else {

				$result		= 'warning';
				$message	= $this->args['page']['update']['messages']['notfound'];

			}
			
			$retorno['result']	= $result;
			$retorno['message']	= $message;

			$this->args['page']['retorno'] = $retorno;

			return view('layouts/returns', $this->args);

		}

		public function destroy($id = null) {

			$this->args['page']['title'] = 'Excluir tipo de pet';
			if ($id) {

				$item = DB::table($this->args['page']['table'])->select('id')->where('id', '=', $id)->first();
				if(count($item) >= 1) {

					$destroy = DB::table($this->args['page']['table'])->where('id', $id)->delete();
					if ($destroy >= 1) {

						$result	= 'success';

					} else {

						$result	= 'danger';

					}

					$message = $this->args['page']['destroy']['messages']['single'][$result];


				} else {

					$result		= 'warning';
					$message	= $this->args['page']['destroy']['messages']['single']['notfound'];

				}

			} else {
				
				$id		=	request('id');
				$total	=	count($id);
				if ($total >= 1) {

					$count	=	0;
					foreach ($id as $i) {

						$item = DB::table($this->args['page']['table'])->select('id')->where('id', '=', $i)->first();
						if(count($item) >= 1) {

							$destroy = DB::table($this->args['page']['table'])->where('id', $i)->delete();
							if ($destroy >= 1) {

								$count++;

							}

						} else {

							$count++;

						}

					}

					if ($count >= $total) {

						$result		= 'success';
						$message	= $this->args['page']['destroy']['messages']['multiple']['success'];

					} else {

						if ($count == 0) {

							$result		= 'danger';
							$message	= $this->args['page']['destroy']['messages']['multiple']['danger'];

						} else {

							$result		= 'warning';
							$message	= $this->args['page']['destroy']['messages']['multiple']['parse'];

						}

					}

				} else {

					$result		= 'warning';
					$message	= $this->args['page']['destroy']['messages']['multiple']['empty'];

				}

			}

			$retorno['result']	= $result;
			$retorno['message']	= $message;

			$this->args['page']['retorno']	=	$retorno;

			return view('layouts/returns', $this->args);

		}

	}
