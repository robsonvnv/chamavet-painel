<?php
	
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Providers\AutomatorServiceProvider as Automator;
	use DB;


	class VeterinarioController extends Controller {
		
		private	$args;
		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */

		public function __construct() {

			$this->args	=	array(

				'page'	=>	array(

					'name'		=> 'Vaterinários',
					'base'		=> 'veterinarios/',
					'table'		=> 'veterinarios',
					'redirect'	=> '5000',
					'list'		=> array(

						'enabled'	=> true,
						'title'		=> 'Listar veterinários',
						'search'	=> true,
						'results'	=> 15

					),
					'create'	=> array(

						'enabled'	=> true,
						'action'	=> 'store',
						'title'		=> 'Cadastrar veterinário',
						'messages'	=> array(

							'success'	=> 'Veterinário cadastrado com sucesso!',
							'danger'	=> 'O veterinário não pode ser cadastrado, tente novamente mais tarde'

						)

					),
					'update'	=> array(

						'enabled'	=> true,
						'action'	=> 'save',
						'messages'	=> array(

							'notfound'	=> 'O veterinário que você está tentando editar não foi encontrado!',
							'success'	=> 'Veterinário editado com sucesso!',
							'danger'	=> 'O veterinário não pode ser editado, tente novamente mais tarde'

						)

					),
					'destroy'	=> array(

						'enabled'	=> true,
						'title'		=> 'Excluir veterinário',
						'messages'	=> array(

							'single'	=> array(

								'success'	=> 'O veterinário foi excluido com sucesso!',
								'danger'	=> 'O veterinário não pode ser excluido!',
								'notfound'	=> 'O veterinário que você está tentando excluir não foi encontrado!'

							),
							'multiple'	=> array(

								'success'	=> 'Todos os veterinários selecionados foram excluidos com sucesso!',
								'parse'		=> 'Um ou mais veterinários não puderam ser excluidos!',
								'danger'	=> 'Os veterinários selecionados não puderam ser excluidos!',
								'empty'		=> 'Por favor selecione ao menos um veterinário para ser excluido!'

							)

						)

					),
					'cols'	=> array(

						'name'	=>	array(

							'type'		=> 'text',
							'paginator'	=> array(

								'enabled'	=> true,
								'label'		=> 'Nome',
								'search'	=> true,
								'order'		=> true

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'message'		=> 'Por favor preencha o campo Nome',
								'label'			=> 'Nome',
								'container'		=> '6',
								'clear'			=> false,
								'required'		=> true,
								'maxlength'		=> 30,

							)

						),
						'email'	=>	array(

							'type'		=> 'email',
							'paginator'	=> array(

								'enabled'	=> true,
								'label'		=> 'Email',
								'search'	=> true,
								'order'		=> true

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'label'			=> 'Email',
								'message'		=> 'Por favor preencha o campo E-mail',
								'container'		=> '6',
								'clear'			=> true,
								'required'		=> true

							)

						),
						'gender'	=>	array(

							'type'		=> 'select',
							'options'	=> array(

								'male'		=> 'Masculino',
								'female'	=> 'Feminino'

							),
							'paginator'	=> array(

								'enabled'	=> false

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'label'			=> 'Sexo',
								'message'		=> 'Por favor Selecione uma opção para o campo Sexo',
								'container'		=> '6',
								'clear'			=> false,
								'required'		=> true

							)

						),
						'phone'	=>	array(

							'type'		=> 'text',
							'paginator'	=> array(

								'enabled'	=> false

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'label'			=> 'Telefone',
								'message'		=> 'Por favor preencha o campo Telefone',
								'container'		=> '6',
								'clear'			=> true,
								'required'		=> true

							)

						),
						'status'	=>	array(

							'type'		=> 'status',
							'paginator'	=> array(

								'enabled'	=> true,
								'label'		=> 'Status',
								'search'	=> true,
								'order'		=> true

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'label'			=> 'Status',
								'message'		=> 'Por favor Selecione uma opção para o campo Status',
								'container'		=> '6',
								'clear'			=> true,
								'required'		=> true

							)

						),
						'password'	=>	array(

							'type'		=> 'password',
							'paginator'	=> array(

								'enabled'	=> false

							),
							'form-maker'	=> array(

								'enabled'		=> true,
								'label'			=> 'Senha',
								'message'		=> 'Por favor preencha o campo Senha',
								'container'		=> '6',
								'clear'			=> true,
								'required'		=> true

							)

						)

					)

				)

			);

		}


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		 */


		public function index(Request $request) {

			$this->args['page']['title']		= 'Listar veterinários';

			$paginator							= array(

				'table'		=> $this->args['page']['table'],
				'cols'		=> $this->args['page']['cols'],
				'results'	=> $this->args['page']['list']['results'],
				'filters'	=> $request->all()

			);

			$this->args['page']['paginator']	= Automator::paginator($paginator);


			return view('layouts/paginator', $this->args);

		}


		public function create() {

			$this->args['page']['title']		=	'Cadastrar veterinário';

			$formMaker							= array(

				'table'		=> $this->args['page']['table'],
				'cols'		=> $this->args['page']['cols'],
				'action'	=> $this->args['page']['create']['action'],
				'id'		=> 0

			);

			$this->args['page']['form-maker']	= Automator::formMaker($formMaker);
			$this->args['page']['create']		=	false;
			$this->args['page']['destroy']		=	false;

			return view('layouts/form-maker', $this->args);

		}


		public function store(Request $request) {

			$this->args['page']['title'] = 'Cadastrar veterinário';
			$fields	= array(

				'name'			=>	$request->input('name'),
				'email'			=>	$request->input('email'),
				'password'		=>	$request->input('password'),
				'gender'		=>	$request->input('gender'),
				'phone'			=>	$request->input('phone'),
				'status'		=>	$request->input('status'),
				'created_at'	=>	date('Y-m-d h:i:s')

			);

			$error = '';
			foreach ($fields as $key => $value) {
				
				if ($error == '') {

					if ($value == '') {

						$error	= $this->args['page']['cols'][$key]['form-maker']['message'];

					}

				}

			}

			if ($error == '') {

				$email	=	DB::table($this->args['page']['table'])->select('email')->where('email', '=', $fields['email'])->first();
				if (count($email) <= 0) {

					$fields['password']	=	bcrypt($fields['password']);
					$insert	=	DB::table($this->args['page']['table'])->insert($fields);
					if ($insert >= 1) {

						$result	= 'success';

					} else {

						$result	= 'danger';

					}
					
					$retorno['message']	= $this->args['page']['create']['messages'][$result];

				} else {

					$result				= 'warning';
					$retorno['message']	= 'Este endereço de e-mail já está sendo utilizado em nosso sistema!';

				}

			} else {
				
				$result				= 'warning';
				$retorno['message']	= $error;
				
			}

			$retorno['result']				= $result;
			$this->args['page']['retorno']	= $retorno;
			
			return view('layouts/returns', $this->args);

		}


		public function update($id) {

			$this->args['page']['title'] = 'Editar usuário';
			$this->args['page']['cols']['password']['form-maker']['required'] = false;

			$formMaker							= array(

				'table'		=> $this->args['page']['table'],
				'cols'		=> $this->args['page']['cols'],
				'action'	=> $this->args['page']['update']['action'],
				'id'		=> $id

			);

			$this->args['page']['form-maker']	= Automator::formMaker($formMaker);

			return view('layouts/form-maker', $this->args);

		}


		public function save(Request $request) {

			$this->args['page']['title'] = 'Editar usuário';
			$id		= $request->input('id');
			if ($id) {

				$search	= DB::table($this->args['page']['table'])->select('email')->where('id', '=', $id)->first();
				if (count($search) >= 1) {

					$fields	= array(
	
						'name'			=>	$request->input('name'),
						'gender'		=>	$request->input('gender'),
						'phone'			=>	$request->input('phone'),
						'status'		=>	$request->input('status'),
						'updated_at'	=>	date('Y-m-d h:i:s')
	
					);

					$error = '';
					if ($search->email != $request->input('email')) {

						$email	=	DB::table($this->args['page']['table'])->select('id')->where('email', '=', $request->input('email'))->first();
						if (count($email) >= 1) {

							$error	= 'O email inserido já está sendo utilizado por outro usuário.';

						} else {

							$fields['email'] = $request->input('email');

						}

					}


					foreach ($fields as $key => $value) {
							
						if ($error == '') {
	
							if ($value == '') {
	
								$error	= $this->args['page']['cols'][$key]['form-maker']['message'];
	
							}
	
						}
	
					}


					if ($error == '') {

						if ($request->input('password') != '') {

							$fields['password'] = bcrypt($request->input('password'));

						}

						$update	= DB::table($this->args['page']['table'])->where('id', $id)->update($fields);
						if ($update >= 1) {

							$result = 'success';

						} else {

							$result = 'danger';

						}

						$message	= $this->args['page']['update']['messages'][$result];

					} else {

						$result		= 'warning';
						$message	= $error;

					}

				} else {

					$result		= 'warning';
					$message	= $this->args['page']['update']['messages']['notfound'];

				}

			} else {

				$result		= 'warning';
				$message	= $this->args['page']['update']['messages']['notfound'];

			}
			
			$retorno['result']	= $result;
			$retorno['message']	= $message;

			$this->args['page']['retorno'] = $retorno;

			return view('layouts/returns', $this->args);

		}

		public function destroy($id = null) {

			$this->args['page']['title'] = 'Excluir usuário';
			if ($id) {

				$item = DB::table($this->args['page']['table'])->select('id')->where('id', '=', $id)->first();
				if(count($item) >= 1) {

					$destroy = DB::table($this->args['page']['table'])->where('id', $id)->delete();
					if ($destroy >= 1) {

						$result	= 'success';

					} else {

						$result	= 'danger';

					}

					$message = $this->args['page']['destroy']['messages']['single'][$result];


				} else {

					$result		= 'warning';
					$message	= $this->args['page']['destroy']['messages']['single']['notfound'];

				}

			} else {
				
				$id		=	request('id');
				$total	=	count($id);
				if ($total >= 1) {

					$count	=	0;
					foreach ($id as $i) {

						$item = DB::table($this->args['page']['table'])->select('id')->where('id', '=', $i)->first();
						if(count($item) >= 1) {

							$destroy = DB::table($this->args['page']['table'])->where('id', $i)->delete();
							if ($destroy >= 1) {

								$count++;

							}

						} else {

							$count++;

						}

					}

					if ($count >= $total) {

						$result		= 'success';
						$message	= $this->args['page']['destroy']['messages']['multiple']['success'];

					} else {

						if ($count == 0) {

							$result		= 'danger';
							$message	= $this->args['page']['destroy']['messages']['multiple']['danger'];

						} else {

							$result		= 'warning';
							$message	= $this->args['page']['destroy']['messages']['multiple']['parse'];

						}

					}

				} else {

					$result		= 'warning';
					$message	= $this->args['page']['destroy']['messages']['multiple']['empty'];

				}

			}

			$retorno['result']	= $result;
			$retorno['message']	= $message;

			$this->args['page']['retorno']	=	$retorno;

			return view('layouts/returns', $this->args);

		}

	}
