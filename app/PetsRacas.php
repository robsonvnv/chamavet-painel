<?php

	namespace App;


	class PetsRacas {

		protected $table = 'pets_racas';
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'tipo_id', 'name', 'status'
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */

		public $timestamps = false;
		
	}
