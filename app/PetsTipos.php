<?php

	namespace App;


	class PetsTipos {

		protected $table = 'pets_tipos';
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'name', 'status'
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */

		public $timestamps = false;
		
	}
