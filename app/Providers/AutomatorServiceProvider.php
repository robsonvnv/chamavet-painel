<?php

	namespace App\Providers;

	use Illuminate\Support\ServiceProvider;
	use DB;

	class AutomatorServiceProvider extends ServiceProvider {
		
		public $masks;


		/**
		 * Bootstrap the application services.
		 *
		 * @return void
		 */
		
		public function boot() {}

		/**
		* Register the application services.
		*
		* @return void
		*/
		public function register() {


		}


		static function mask($tipo, $funcao, $valores = '') {

			$classe	=	ucfirst($tipo) . 'Mask';
			if (file_exists(base_path('app/Automator/') . $classe . '.php')) {

				require_once(base_path('app/Automator/') . $classe . '.php');
				if(class_exists($classe)) {

					$classe = new $classe();
					if ($valores == '') {

						$retorno	=	array(

							'result'	=>	true,
							'mask'		=>	$classe->$funcao()

						);

					} else {

						$retorno	=	array(

							'result'	=>	true,
							'mask'		=>	$classe->$funcao($valores)

						);

					}

				} else {

					$retorno	= array(

						'result'	=>	false,
						'message'	=>	'A classe da mascara solicitada não existe.'

					);

				}

			} else {

				$retorno	= array(

					'result'	=>	false,
					'message'	=>	'A mascara solicitada não existe.'

				);

			}

			return $retorno;

		}


		static function paginator($args) {

			$table				=	$args['table'];
			$cols				=	$args['cols'];
			$filters			=	$args['filters'];
			$results			=	$args['results'];

			foreach ($cols as $col => $value) {
				
				if ($value['paginator']['enabled'] == true) {

					$retorno['cols'][$col]	= array(

						'type'		=> $value['type'],
						'label'		=> $value['paginator']['label'],
						'search'	=> $value['paginator']['search'],
						'order'		=> $value['paginator']['order']

					);

				}

			}

			unset($filters['_token']);
			unset($filters['colls']);
			unset($filters['value']);

			if (count($filters) >= 1) {

				foreach ($filters as $key => $value) {
					
					if($key != 'results' && $key != 'page') {

						$campo	=	str_replace('page-header-form-coll-', '', $key);
						if ($value != '') {

							$condicao	=	AutomatorServiceProvider::mask($retorno['cols'][$campo]['type'], 'filter', $value);
							if ($condicao['result'] == true) {

								$filter[$campo]	=	array(
									
									'condicao'	=>	$condicao['mask']['condicao'],
									'prefix'	=>	$condicao['mask']['prefix'],
									'suffix'	=>	$condicao['mask']['suffix'],
									'valor'		=>	$condicao['mask']['value']

								);

							}
							
						}

					}

				}

				if (isset($filters['results'])) {
					
					$npp = $filters['results'];

				} else {
					
					$npp = $results;

				}

			} else {
				
				$npp = $results;

			}

			if (isset($filter)) {


				$condicoes	=	[];
				foreach ($filter as $key => $value) {
					
					array_push($condicoes, [$key, $value['condicao'], $value['prefix'] . $value['valor'] . $value['suffix']]);

				}


				$query	= DB::table($table)->where($condicoes)->paginate($npp);

			} else {
				
				$query	= DB::table($table)->paginate($npp);

			}

			$retorno['data']['total']	= $query->total();
			$retorno['data']['links']	= $query->links();
			if ($retorno['data']['total'] >= 1) {

				foreach ($query as $key => $value) {
					
					$retorno['data']['results'][$key]['id'] = $value->id;
					foreach ($retorno['cols'] as $chave => $valor) {
						
						$mask = AutomatorServiceProvider::mask($valor['type'], 'paginator', $value->$chave);
						if ($mask['result'] == true) {

							$retorno['data']['results'][$key][$chave] = $mask['mask'];

						} else {
							
							$retorno['data']['results'][$key][$chave] = '- - -';

						}
						
					}

				}

			} else {

				$retorno['data']['results']	= array();

			}

			return $retorno;

		}




		static function paginator2($args) {

			$table				=	$args['table'];
			$cols				=	$args['cols'];
			$filters			=	$args['filters'];
			$results			=	$args['results'];

			foreach ($cols as $col => $value) {

				if ($value['paginator']['enabled'] == true) {

					$retorno['cols'][$col]['type'] = $value['type'];
					unset($value['paginator']['enabled']);
					foreach ($value['paginator'] as $key => $valor) {
						
						$retorno['cols'][$col][$key] = $valor;

					}

				}

			}

			if (isset($filters)) {

				foreach ($retorno['cols'] as $key => $value) {
					
					if ($value['search'] == true) {

						if (isset($filters['page-header-form-coll-' . $key])) {

							$filtros[$key] = $filters['page-header-form-coll-' . $key];

						}

					}

				}

				if (isset($filters['results'])) {
					
					$npp = $filters['results'];

				} else {
					
					$npp = $results;

				}

			} else {

				$npp = $results;

			}

			if (count($filtros) >= 1) {

				foreach ($filtros as $key => $value) {
					
					$condicao	=	AutomatorServiceProvider::mask($retorno['cols'][$key]['type'], 'filter', $value);
					if($condicao == true) {

						foreach ($condicao['mask'] as $chave => $valor) {
							
							$filter[$key][$chave]	= $valor;
						}

					}

				}

			}

			if (isset($filter)) {


				$condicoes	=	[];
				foreach ($filter as $key => $value) {
					
					array_push($condicoes, [$key, $value['condicao'], $value['prefix'] . $value['value'] . $value['suffix']]);

				}


				$query	= DB::table($table)->where($condicoes)->paginate($npp);

			} else {
				
				$query	= DB::table($table)->paginate($npp);

			}

			$retorno['data']['total']	= $query->total();
			$retorno['data']['links']	= $query->links();
			if ($retorno['data']['total'] >= 1) {

				foreach ($query as $key => $value) {
					
					$retorno['data']['results'][$key]['id'] = $value->id;
					foreach ($retorno['cols'] as $chave => $valor) {
						
						$mask = AutomatorServiceProvider::mask($valor['type'], 'paginator', $value->$chave);
						if ($mask['result'] == true) {

							$retorno['data']['results'][$key][$chave] = $mask['mask'];

						} else {
							
							$retorno['data']['results'][$key][$chave] = '- - -';

						}
						
					}

				}

			} else {

				$retorno['data']['results']	= array();

			}


			echo '<pre>';

				print_r($args);
				
			echo '</pre>';

			return $retorno;

		}




		

		static function formMaker($args) {

			$table				=	$args['table'];
			$cols				=	$args['cols'];
			$id					=	$args['id'];
			$retorno['action']	=	$args['action'];

			foreach ($cols as $col => $value) {
				
				if ($value['form-maker']['enabled'] == true) {

					$retorno['cols'][$col]['type']		= $value['type'];
					$retorno['cols'][$col]['default']	= '';
					$retorno['cols'][$col]['maxlength']	= '';
					if (isset($value['options'])) {

						$retorno['cols'][$col]['options']	= $value['options'];

					} else {
						
						$retorno['cols'][$col]['options']	= '';

					}
					
					foreach ($value['form-maker'] as $key => $val) {
						
						if ($key != 'enabled') {

							$retorno['cols'][$col][$key] = $val;

						}

					}

				}

			}

			if ($id >= 1) {

				$query	= DB::table($table)->select('*')->where('id', '=', $id)->get();
				foreach ($retorno['cols'] as $key => $value) {
					
					$retorno['cols'][$key]['default'] = $query[0]->$key;

				}

			}


			foreach ($retorno['cols'] as $key => $value) {
				
				$formMaker = array(

					'col'		=> $key,
					'default'	=> $retorno['cols'][$key]['default'],
					'required'	=> $retorno['cols'][$key]['required'],
					'options'	=> $retorno['cols'][$key]['options'],
					'maxlength'	=> $retorno['cols'][$key]['maxlength'],

				);
				$mask = AutomatorServiceProvider::mask($retorno['cols'][$key]['type'], 'formMaker', $formMaker);
				if ($mask['result'] == true) {

					$retorno['cols'][$key]['html']	=	$mask['mask'];

				}

			}

			$retorno['id'] = $id;

			return $retorno;

		}

	}
