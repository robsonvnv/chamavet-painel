<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersChamadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_chamados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pet')->unsigned();
            $table->integer('endereco')->unsigned();
            $table->integer('servico')->unsigned();
            $table->integer('card')->unsigned();
            $table->float('price', 0, 2);
            $table->longText('observacoes')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();

            $table->foreign('pet')->references('id')->on('users_pets');
            $table->foreign('endereco')->references('id')->on('users_adresses');
            $table->foreign('servico')->references('id')->on('users_servicos');
            $table->foreign('card')->references('id')->on('users_cards');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_chamados');
    }
}
