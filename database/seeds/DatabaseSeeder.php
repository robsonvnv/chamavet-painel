<?php

	use Illuminate\Database\Seeder;

	class DatabaseSeeder extends Seeder {
		
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */


		public function run() {

			$this->call(UsersTableSeeder::class);
			$this->call(ServicosTableSeeder::class);
			$this->call(PetsTiposTableSeeder::class);
			$this->call(PetsRacasTableSeeder::class);
			$this->call(UsersServicosTableSeeder::class);

		}

	}
