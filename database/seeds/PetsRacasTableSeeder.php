<?php

	use Illuminate\Database\Seeder;

	class PetsRacasTableSeeder extends Seeder {
		
		/**
		* Run the database seeds.
		*
		* @return void
		*/

		public function run() {

			DB::table('pets_racas')->insert([
				'name'			=> 'Persa',
				'tipo'			=> '1',
				'status'		=> '1',
				'created_at'	=> date('Y-m-d h:i:')
			]);


			DB::table('pets_racas')->insert([
				'name'			=> 'Angorá',
				'tipo'			=> '1',
				'status'		=> '1',
				'created_at'	=> date('Y-m-d h:i:')
			]);

			
			DB::table('pets_racas')->insert([
				'name'			=> 'Siames',
				'tipo'			=> '1',
				'status'		=> '0',
				'created_at'	=> date('Y-m-d h:i:')
			]);


			DB::table('pets_racas')->insert([

				'name'			=> 'Poodle',
				'tipo'			=> '2',
				'status'		=> '1',
				'created_at'	=> date('Y-m-d h:i:')

			]);


			DB::table('pets_racas')->insert([
				'name'			=> 'Rottweiler',
				'tipo'			=> '2',
				'status'		=> '1',
				'created_at'	=> date('Y-m-d h:i:')
			]);


			DB::table('pets_racas')->insert([
				'name'			=> 'Yorkshire',
				'tipo'			=> '2',
				'status'		=> '0',
				'created_at'	=> date('Y-m-d h:i:')
			]);

		}
	
	}
