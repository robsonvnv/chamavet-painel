<?php

use Illuminate\Database\Seeder;

class PetsTiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pets_tipos')->insert([
			'name'			=> 'Gato',
			'status'		=> '1',
			'created_at'	=> date('Y-m-d h:i:')
		]);


		DB::table('pets_tipos')->insert([
			'name'			=> 'Cachorro',
			'status'		=> '1',
			'created_at'	=> date('Y-m-d h:i:')
		]);
    }
}
