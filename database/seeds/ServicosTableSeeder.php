<?php

	use Illuminate\Database\Seeder;

	class ServicosTableSeeder extends Seeder {
		
		/**
		* Run the database seeds.
		*
		* @return void
		*/

		public function run() {

			DB::table('servicos')->insert([
				'name' => 'Vacina',
				'price' => '50',
				'status' => '1',
				'created_at' => date('Y-m-d h:i:')
			]);


			DB::table('servicos')->insert([
				'name' => 'Consulta',
				'price' => '100',
				'status' => '1',
				'created_at' => date('Y-m-d h:i:')
			]);


			DB::table('servicos')->insert([
				'name' => 'Cirurgia',
				'price' => '1000',
				'status' => '1',
				'created_at' => date('Y-m-d h:i:')
			]);

		}

	}
