<?php

	use Illuminate\Database\Seeder;

	class UsersServicosTableSeeder extends Seeder {
		
		/**
		* Run the database seeds.
		*
		* @return void
		*/

		public function run() {

			DB::table('users_servicos')->insert([
				'user' => '2',
				'servico' => '1',
				'created_at' => date('Y-m-d h:i:')
			]);


			DB::table('users_servicos')->insert([
				'user' => '2',
				'servico' => '2',
				'created_at' => date('Y-m-d h:i:')
			]);

		}

	}
