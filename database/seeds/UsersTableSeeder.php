<?php

	use Illuminate\Database\Seeder;
	use Illuminate\Database\Eloquent\Model;

	class UsersTableSeeder extends Seeder {
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {
			
			DB::table('users')->insert([
				'name'			=> 'Robson Vieira',
				'email'			=> 'robson@resultadosmobile.com.br',
				'gender'		=> 'male',
				'level'			=> 1,
				'password'		=> bcrypt('admin'),
				'created_at'	=> date('Y-m-d h:i:')
			]);


			DB::table('users')->insert([
				'name'			=> 'Jason Bourne',
				'email'			=> 'contato@resultadosmobile.com.br',
				'gender'		=> 'male',
				'level'			=> 2,
				'password'		=> bcrypt('admin'),
				'created_at'	=> date('Y-m-d h:i:')
			]);

		}

	}
