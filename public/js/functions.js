function AutomatorSearch(endereco, token, objeto) {

	$('#page-header-form-input').html('Carregando...');

	var valor	=	$('option:selected', objeto).val();
	if (valor == '') {

		$('#page-header-form-input').html('<input type="text" class="form-control" value="Selecione um campo para filtrar os resultados" disabled />');

	} else {

		var tipo	=	$('option:selected', objeto).attr('data-type');

		AutomatorLoadMask(endereco, token, tipo, 'search');

	}

}


function AutomatorLoadMask(endereco, token, tipo, funcao) {

	$.ajax({

		url: endereco,
		method: 'POST',
		headers: {
			'X-CSRF-TOKEN': token
		},
		data: { tipo: tipo, funcao: funcao },
		success: function(result) {

			$('#page-header-form-input').html(result);

		}

	});

}