@extends('layouts.app')
@section('content')
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
	<script>

		(function($, window, undefined) {
			
			//is onprogress supported by browser?
			var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

			//If not supported, do nothing
			if (!hasOnProgress) {
				
				return;
			
			}

			//patch ajax settings to call a progress callback
			var oldXHR = $.ajaxSettings.xhr;
			$.ajaxSettings.xhr = function() {
				
				var xhr = oldXHR();
				if(xhr instanceof window.XMLHttpRequest) {
					
					xhr.addEventListener('progress', this.progress, false);
				
				}

				if(xhr.upload) {
					
					xhr.upload.addEventListener('progress', this.progress, false);
				}

				return xhr;
			
			};
		
		})(jQuery, window);
		
		$(function() {

			$('.geral').keyup(function() {

				var total	= $('.geral').length;
				var ok		= 0;
				$('.geral').each(function(index, value) {

					if($(value).val() != '') {

						ok++;

					}

				}).promise().done(function() {

					if (ok >= total) {

						$('#tab-geral > a').css('background-color', 'green');
						$('#tab-geral > a').css('color', '#FFFFFF');

					} else {

						$('#tab-geral > a').removeAttr('style');

					}

				});

			});

			$('#database-table').on('change', function() {

				$(this).prop('disabled', true);
				var valor = $(this).val();
				if (valor == '') {

					$('#database-cols').fadeOut(500, function() {

						$('#database-cols').html('Por favor selecione uma tabela para selecionar seu campos!');
						setTimeout(function() {

							$('#database-cols').fadeIn(500);

						}, 500);

					});

				} else {

					$('#database-cols').fadeOut(500, function() {

						$('#database-cols').html("<div class='progress'><div id='database-cols-progress' class='progress-bar progress-bar-info progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'><span class='sr-only'>0% Complete</span></div></div>");
						$('#database-cols').append("<br />");
						$('#database-cols').append("<h4>Carregando ...</h4>");
						setTimeout(function() {

							$('#database-cols').fadeIn(500, function() {

								$.ajax({
									
									method: 'POST',
									url: "{{ url('/automator/cols') }}",
									dataType: 'json',
									headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
									data: { table: valor },
									success: function(retorno) {

										$('#database-cols-progress').css('width', '100%');
										setTimeout(function () {

											$('#database-cols').fadeOut(500, function() {

												if (retorno.result == false) {

													$('#database-cols').html('<div class="alert alert-danger text-center"><h3>Erro</h3><br />Parece que ocorreu algum erro ao localizar os campos desta tabela, por favor tente novamente.</div>');

												} else {

													var codigo  =	'<table class="table table-bordered table-hover">';
														
														codigo		+=	'<thead>';
															
															codigo		+=	'<tr>';
																
																codigo		+=	'<th class="text-center"><input type="checkbox" id="database-cols-checkall" /></th>';
																codigo		+=	'<th>Coluna</th>';
																codigo		+=	'<th>Tipo</th>';
														
														codigo		+=	'</tr>';
													
													codigo		+=	'</thead>';
													codigo		+=	'<tbody>';
													$.each(retorno.cols, function(indice, valor) {
														
														codigo +=	'<tr>';
															
															codigo	+=	'<td class="text-center"><input type="checkbox" class="database-col" name="database[cols]" value="' + valor + '" /></td>';
															codigo	+=	'<td>' + valor + '</td>';
															codigo	+=	'<td>';

																codigo	+=	'<select class="form-control">';

																	codigo	+=	'<option value="">- Selecione -</option>';

																codigo	+=	'</select>';

															codigo	+=	'</td>';

														codigo +=	'</tr>';

													// }).promise().done(function() {


													});
														codigo += '</body>';

													codigo += '</table>';

													$('#database-cols').html(codigo);

												}

												setTimeout(function() {

													$('#database-cols').fadeIn(500, function() {

														$('#database-table').prop('disabled', false);

													});

												}, 500);

											});

										}, 2000);

									},
									error: function() {

										$('#database-cols').html('<div class="alert alert-danger text-center"><h3>Erro</h3><br />Parece que ocorreu algum erro ao localizar os campos desta tabela, por favor tente novamente.</div>');

									},
									progress: function(e) {

										//make sure we can compute the length
										if(e.lengthComputable) {

											//calculate the percentage loaded
											var pct = (e.loaded / e.total) * 100;

											//log percentage loaded
											$('#database-cols-progress').css('width', pct.toPrecision(3) + '%');

										} else {
											
											console.warn('Content Length not reported!');

										}

									}

								});

							});
						}, 500);

					});

				}

			});

		});
		
	</script>
	<div class="container">

		<div class="row">

			<div class="col-xs-12 text-center">

				<h2>Automator</h2>

			</div>

		</div>
		<br />
		<div class="row">
			
			<div class="col-xs-12 text-center">

				<h3>Utilize o formulário abaixo para criar um controlador utilizando o automator.</h3>

			</div>

		</div>
		<br />
		<br />
		<div class="row">

			<div class="col-xs-12 col-md-2">

				<ul class="nav nav-pills nav-stacked" role="tablist">
					
					<li id="tab-geral" role="presentation" class="active"><a href="#geral" aria-controls="geral" role="tab" data-toggle="tab">Geral</a></li>
					<li id="tab-database" role="presentation"><a href="#database" aria-controls="database" role="tab" data-toggle="tab">Database</a></li>
					<li role="presentation"><a href="#listagem" aria-controls="listagem" role="tab" data-toggle="tab">Listagem</a></li>
					<li role="presentation"><a href="#inserir" aria-controls="inserir" role="tab" data-toggle="tab">Inserir</a></li>
					<li role="presentation"><a href="#editar" aria-controls="editar" role="tab" data-toggle="tab">Editar</a></li>
					<li role="presentation"><a href="#excluir" aria-controls="excluir" role="tab" data-toggle="tab">Excluir</a></li>

				</ul>

			</div>
			<div class="col-xs-12 col-md-10">

				<form method="POST" action="{{ url('/automator/generate') }}">
					<div class="tab-content">

						<div role="tabpanel" class="tab-pane active" id="geral">

							<h4>Geral</h4>
							<div class="row">

								<div class="col-xs-12 col-md-6">

									<label for="geral-controller">Controller</label>
									<input type="text" id="geral-controller" name="geral[controller]" class="form-control geral" placeholder="Ex: UsersController" />

								</div>

							</div>
							<div class="row">
								
								<div class="col-xs-12 col-md-6">

									<label for="geral-nome">Nome</label>
									<input type="text" id="geral-nome" name="geral[name]" class="form-control geral" placeholder="Ex: Usuários" />

								</div>

							</div>
							<div class="row">
								
								<div class="col-xs-12 col-md-6">

									<label for="geral-base">Base</label>
									<input type="text" id="geral-base" name="geral[base]" class="form-control geral" placeholder="Ex: /usuarios" />

								</div>
								<div class="col-xs-12 col-md-6">

									<label for="geral-redirect">Timou de redirect <small>(segundos)</small></label>
									<input type="number" id="geral-redirect" name="geral[redirect]" class="form-control geral" value="1" min="1" max="10" />

								</div>

							</div>

						</div>
						<div role="tabpanel" class="tab-pane" id="database">

							<h4>Database</h4>
							<div class="row">

								<div class="col-xs-12 col-md-5">
									
									<label for="database-table">Tabela</label>
									<select id="database-table" name="database[table]" class="form-control">

										<option value="">- Selecione -</option>
										@foreach($tables as $table)

											<option value="{{ $table }}">{{ $table }}</option>

										@endforeach

									</select>

								</div>

							</div>
							<br />
							<div class="row">

								<div id="database-cols" class="col-xs-12 text-center">
									
									Por favor selecione uma tabela para selecionar seu campos!

								</div>

							</div>

						</div>
						<div role="tabpanel" class="tab-pane" id="listagem">

							<h4>Listagem</h4>

						</div>
						<div role="tabpanel" class="tab-pane" id="inserir">

							<h4>Inserir</h4>

						</div>
						<div role="tabpanel" class="tab-pane" id="editar">

							<h4>Editar</h4>

						</div>
						<div role="tabpanel" class="tab-pane" id="excluir">

							<h4>Excluir</h4>

						</div>

					</div>
				</form>

			</div>

		</div>

	</div>

@endsection