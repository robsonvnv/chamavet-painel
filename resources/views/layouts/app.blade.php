<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">

        #app { margin-bottom: 50px; }
        .dropdown-menu { min-width: 180px; }
        .dropdown-menu > li > b { padding: 3px 10px; }

        #paginator th,
        #paginator td { vertical-align: middle; }

        #paginator-buttons > a,
        #paginator-buttons > input { margin-bottom: 20px; }

        label > span.required { color: red; font-size: 22px; }
        .form-control { margin-bottom: 30px; }

    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pets <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><b>Tipos</b></li>
                                <li><a href="{{ url('/pets/tipos') }}">Listar tipos de pets</a></li>
                                <li><a href="{{ url('/pets/tipos/create') }}">Cadastrar tipo de pet</a></li>
                                <li role="separator" class="divider"></li>
                                <li><b>Raças</b></li>
                                <li><a href="{{ url('/pets/racas') }}">Listar raças</a></li>
                                <li><a href="{{ url('/pets/racas/create') }}">Cadastrar raça</a></li>

                            </ul>

                        </li>
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Serviços <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><a href="{{ url('/servicos') }}">Listar serviços</a></li>
                                <li><a href="{{ url('/servicos/create') }}">Cadastrar serviço</a></li>

                            </ul>

                        </li>
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Clientes <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><b>Usuários</b></li>
                                <li><a href="{{ url('/clientes/usuarios') }}">Listar usuários</a></li>
                                <li><a href="{{ url('/clientes/usuarios/create') }}">Cadastrar usuário</a></li>

                            </ul>

                        </li>
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Veterinário <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><a href="{{ url('/veterinarios') }}">Listar usuários</a></li>
                                <li><a href="{{ url('/veterinarios/create') }}">Cadastrar usuário</a></li>

                            </ul>

                        </li>
                        <li class="dropdown">

                            <a href="{{ url('/automator')}}">Automator</a>

                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
