@extends('layouts.app')
@section('content')
	
	<div class="container">
		
		@include('layouts/header', $page)
		@if(($page['list']['enabled'] == true) || ($page['destroy']['enabled'] == true))

			<div class="row">

				<div class="col-xs-12">

					@if($page['list']['enabled'] == true)

						<a href="{{ url($page['base']) }}" class="btn btn-default">{{ $page['list']['title'] }}</a>

					@endif
					@if($page['create']['enabled'] == true)

						<a href="{{ url($page['base']) }}/create" class="btn btn-primary">{{ $page['create']['title'] }}</a>

					@endif
					@if($page['destroy']['enabled'] == true)

						<script type="text/javascript">
							
							$(function() {

								$("#automator-destroy").click(function() {

									if (!confirm("Tem certeza de que deseja excluir este registro? Esta ação não poderá ser desfeita futuramente.")) {

										return false;

									}

								});

							});

						</script>
						<a id="automator-destroy" href="{{ url($page['base']) }}/destroy/{{ $page['form-maker']['id'] }}" class="btn btn-danger">{{ $page['destroy']['title'] }}</a>

					@endif
					
				</div>

			</div>
			<br />

		@endif
		<form method="POST" action="{{ url($page['base'] . $page['form-maker']['action']) }}">

			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{ $page['form-maker']['id'] }}" />
			<div class="row">

				@foreach($page['form-maker']['cols'] as $col => $params)

					<div class="col-xs-12 col-md-{{ $params['container'] }}">

						<label for="{{ $col }}">
							
							{{ $params['label'] }}
							@if($params['required'] == true)

								<span class="required">*</span>

							@endif

						</label>
						{!! $params['html'] !!}

					</div>

					@if($params['clear'] == true)

						</div>
						<div class="row">

					@endif

				@endforeach

			</div>
			<br />
			<div class="row">

				<div class="col-xs-12 col-md-4 col-md-offset-4">
					
					<input type="submit" value="Salvar" class="btn btn-success btn-block" />

				</div>
			</div>

		</form>

	</div>

@endsection