<div class="page-header">

	@if($page['list']['search'] == true)

		<script type="text/javascript">
			
			$(function() {

				$("#page-header-form").submit(function() {

					var coll = $('select[name="colls"]').val();
					var valor = $('#value').val();
					var filter = $('#page-header-form-filter').val();
					if (filter == "false") {

						if (coll != '') {

							if (valor != '') {

								$('input[name="page-header-form-coll-' + coll + '"]').val(valor);

							} else {
								
								alert("Por favor insira um valor para filtrar os resultados!");
								$('#value').focus();
								return false;

							}

						} else {

							alert("Por favor selecione ao menos uma campo para filtrar os resultados!");
							$('select[name="colls"]').focus();
							return false;

						}

					}

				});
				


				$('.page-header-form-remove-filter').on('click', function() {

					var coll = $(this).attr('data-coll');
					$('#page-header-form-filter').val(true);
					$('input[name="page-header-form-coll-' + coll + '"]').val("");
					$('#page-header-form').submit();

				});

			});

		</script>
		<form id="page-header-form" method="POST" action="{{ url($base) }}">

			{{ csrf_field() }}

			@foreach($cols as $col => $values)

				@if($values['paginator']['enabled'] == true)

					@if($values['paginator']['search'] == true)

						<input type="hidden" name="page-header-form-coll-{{ $col }}" value="{{ request('page-header-form-coll-' . $col) }}" />

					@endif

				@endif

			@endforeach
			<input type="hidden" id="page-header-form-filter" value="false" />
			<div class="row">

				<div class="col-xs-12 col-md-4">

					<h2>
						
						{{ $name }}
						<br />
						<small>{{ $title }}</small>

					</h2>

				</div>
				<div class="col-xs-5 col-md-2" style="padding-top: 30px;">

					@php $rota = 'AutomatorController@mask'; @endphp
					<select name="colls" class="form-control" onchange="AutomatorSearch('{{ action($rota) }}', '{{ csrf_token() }}', this);">

						<option value="">- Selecione -</option>
						@foreach($cols as $col => $values)

							@if($values['paginator']['enabled'] == true)

								@if($values['paginator']['search'] == true)

									@if(request('page-header-form-coll-' . $col) == "")
										
										<option value="{{ $col }}" data-type="{{ $values['type'] }}">{{ $values['paginator']['label'] }}</option>

									@endif

								@endif
								
							@endif

						@endforeach

					</select>

				</div>
				<div id="page-header-form-input" class="col-xs-7 col-md-4" style="padding-top: 30px;">

					<input type="text" name="value" class="form-control" value="Selecione um campo para filtrar os resultados" disabled />

				</div>
				<div class="col-xs-12 col-md-2" style="padding-top: 30px;">

					<input type="submit" class="btn btn-success btn-block" value="Filtrar" />

				</div>

			</div>

		</form>

	@else

		<h2>
					
			{{ $name }}
			<br />
			<small>{{ $title }}</small>

		</h2>

	@endif

</div>
@php $filtros = 0; @endphp
@foreach($cols as $col => $values)
	
	@if($values['paginator']['enabled'] == true)
		
		@if($values['paginator']['search'] == true)

			@if(request('page-header-form-coll-' . $col) != '')

				@php $filtros++; @endphp

			@endif

		@endif

	@endif

@endforeach
@if ($filtros >= 1)
	
	<div class="row">

		<div class="col-xs-12">

			<h3>Filtros:</h3>
		</div>

	</div>
	<div class="row">

		<div class="col-xs-12 col-md-4">

			<ul class="list-group">

				@foreach($cols as $col => $values)
					
					@if($values['paginator']['enabled'] == true)
					
						@if($values['paginator']['search'] == true)
					
							@if(request('page-header-form-coll-' . $col) != '')


								<li class="list-group-item text-center">

									<h4 class="list-group-item-heading"><b style="top: 8px; position: relative;">{{ $values['paginator']['label'] }}</b><button type="button" data-coll="{{ $col }}" class="btn btn-danger pull-right page-header-form-remove-filter">X</button></h4>
									<div style="clear: both;"></div>
									<p class="list-group-item-text">{{ request('page-header-form-coll-' . $col) }}</p>

								</li>


							@endif

						@endif

					@endif

				@endforeach

			</ul>

		</div>

	</div>
	<br />

@endif