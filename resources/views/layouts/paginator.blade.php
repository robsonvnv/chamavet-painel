@extends('layouts.app')
@section('content')
	
	<div class="container">
		
		@include('layouts/header', $page)
		@php
			
			if($page['list']['search'] == true) {

				$form = 'page-header-form';

			} else {

				$form = 'paginator';

			}

		@endphp
		<script type="text/javascript">
			
			$(function() {

				$('#pagintor-links a').on('click', function() {

					var link = $(this).attr('href');

					$('#{{ $form }}').prop('action', link);
					$('#page-header-form-filter').val(true);

					$('#{{ $form }}').submit();
					return false;

				});

				$(".orderBy").click(function() {

					var col		= $(this).attr('data-col');
					
					return false;

				});

			});

		</script>
		@if($page['destroy']['enabled'] == true)

			<script type="text/javascript">
				
				$(function() {

					$('#paginator-checkall').on('click', function() {

						if ($(this).is(':checked') == true) {

							$('.paginator-item-checkbox').each(function() {

								$(this).prop('checked', true);

							}).promise().done(function() {

								$('#paginator-delete-button').prop('disabled', false);

							});

						} else {

							$('#paginator')[0].reset();
							$('.paginator-item-checkbox').each(function() {

								$(this).attr('checked', false);

							}).promise().done(function() {

								$('#paginator-delete-button').prop('disabled', true);

							});

						}

					});


					$('.paginator-item-col').on('click', function() {

						var item = $(this).parent().attr('data-id');
						var checkbox = $('#paginator-item-checkbox-' + item);
						if (checkbox.is(':checked') == true) {

							checkbox.prop('checked', false);

						} else {
							
							checkbox.prop('checked', true);

						}


						var selecionados = 0;
						$('.paginator-item-checkbox').each(function() {

							if ($(this).is(':checked') == true) {

								selecionados++;

							}

						}).promise().done(function() {

							if(selecionados >= 1) {

								$('#paginator-delete-button').prop('disabled', false);

							} else {
									
								$('#paginator-delete-button').prop('disabled', true);

							}

						});

					});



					$('.paginator-item-checkbox').on('click', function() {

						if ($(this).is(':checked') == true) {

							$(this).prop('checked', true);

						} else {
							
							$(this).prop('checked', false);

						}

						var selecionados = 0;
						$('.paginator-item-checkbox').each(function() {

							if ($(this).is(':checked') == true) {

								selecionados++;

							}

						}).promise().done(function() {

							if(selecionados >= 1) {

								$('#paginator-delete-button').prop('disabled', false);

							} else {
									
								$('#paginator-delete-button').prop('disabled', true);

							}

						});

					});


					$('#paginator').submit(function() {

						if (!confirm("Tem certeza de que deseja excluir este(s) registro(s)? Esta ação não poderá ser desfeita futuramente.")) {

							return false;

						}

					});


					$(".paginator-destroy").click(function() {

						if (!confirm("Tem certeza de que deseja excluir este registro? Esta ação não poderá ser desfeita futuramente.")) {

							return false;

						}

					});

				});

			</script>
			<form id="paginator" method="POST" action="{{ url($page['base']) }}/destroy">

				{{ csrf_field() }}

		@endif

			@if(($page['create']['enabled'] == true) || ($page['destroy']['enabled'] == true))

				<div class="row">

					<div id="paginator-buttons" class="col-xs-12">

						@if($page['create']['enabled'] == true)
							
							<a href="{{ url($page['base']) }}/create" class="btn btn-primary">{{ $page['create']['title'] }}</a>

						@endif
						@if($page['destroy']['enabled'] == true)

							<input id="paginator-delete-button" type="submit" class="btn btn-danger" value="Excluir registro(s) selecionado(s)" disabled />

						@endif
						
					</div>

				</div>

			@endif
			<div class="row">

				<div class="col-xs-12">
					
					Foram encontrados <b>{{ $page['paginator']['data']['total'] }}</b> registro(s).

				</div>

			</div>
			<br />
			<div class="row">
				
				<div class="col-xs-12">

					<table class="table table-bordered table-hover">

						<thead>

							<tr>

								@if($page['destroy']['enabled'] == true)
									
									<th class="text-center"><input type="checkbox" id="paginator-checkall" /></th>

								@endif
								@foreach($page['paginator']['cols'] as $col => $params)

									<th>
										
										@if($params['order'] == true)
											
											<a href="#" class="orderBy" data-col="{{ $col }}">
												
												{{ $params['label'] }}

											</a>

										@else
											
											{{ $params['label'] }}

										@endif

									</th>

								@endforeach
								@if(($page['destroy']['enabled'] == true) || ($page['update']['enabled'] == true))

									<th class="text-center">Ações</th>

								@endif

							</tr>

						</thead>
						<tbody>
							
							@forelse($page['paginator']['data']['results'] as $item)

								<tr data-id="{{ $item['id'] }}">

									@if($page['destroy']['enabled'] == true)
										
										<td class="text-center"><input type="checkbox" class="paginator-item-checkbox" id="paginator-item-checkbox-{{ $item['id'] }}" name="id[]" value="{{ $item['id'] }}" /></td>

									@endif
									@foreach($page['paginator']['cols'] as $col => $params)

										<td class="paginator-item-col">{{ $item[$col] }}</td>

									@endforeach
									@if(($page['destroy']['enabled'] == true) || ($page['update']['enabled'] == true))

										<td class="text-center">
											
											@if($page['destroy']['enabled'] == true)

												<a href="{{ url($page['base']) }}/update/{{ $item['id'] }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>

											@endif
											
											@if($page['destroy']['enabled'] == true)

												<a href="{{ url($page['base']) }}/destroy/{{ $item['id'] }}" class="btn btn-danger paginator-destroy"><span class="glyphicon glyphicon-trash"></span></a>

											@endif

										</td>

									@endif

								</tr>

							@empty
							@endforelse

						</tbody>

					</table>

				</div>

			</div>

		@if($page['destroy']['enabled'] == true)

			</form>

		@endif
		<br />
		<div class="row">

			<div id="pagintor-links" class="col-xs-12 text-center">

				{{ $page['paginator']['data']['links'] }}

			</div>
			
		</div>

	</div>

@endsection