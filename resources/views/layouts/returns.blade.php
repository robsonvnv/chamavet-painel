@extends('layouts.app')
@section('content')
	
	<div class="container">
		
		@include('layouts/header', $page)

		<script type="text/javascript">
			
			$(function() {

				setTimeout(function() {

					window.location.href = "{{ url($page['base']) }}";
				}, "{{ $page['redirect'] }}");

			});

		</script>
		<div class="row">

			<div class="col-xs-12">

				<div class="alert alert-{{ $page['retorno']['result'] }} text-center">

					<h3>
						
						@if($page['retorno']['result'] == 'success')

							Sucesso!

						@elseif($page['retorno']['result'] == 'danger')

							Erro!

						@else

							Atenção!

						@endif
						
					</h3>
					<br />
					{{ $page['retorno']['message'] }}
					<br />
					<br />

				</div>
			
			</div>

		</div>

	</div>

@endsection