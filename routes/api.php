<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Auth::routes();

Route::group(['namespace' => 'api'], function () {

	Route::group(['prefix' => 'clientes'], function () {
		
		Route::post('/register', 'ClientesController@register');
		Route::post('/login', 'ClientesController@login');
		Route::post('/facebook', 'ClientesController@facebook');
		Route::post('/update', 'ClientesController@update')->middleware('auth:api');

		Route::any('/pets/{id?}', 'ClientesPetsController@index')->middleware('auth:api');
		Route::any('/cards/{id?}', 'ClientesCardsController@index')->middleware('auth:api');
		Route::any('/adresses/{id?}', 'ClientesAdressesController@index')->middleware('auth:api');

	});

	Route::group(['prefix' => 'veterinarios'], function () {

		Route::post('/login', 'VeterinariosController@login');
		Route::post('/update', 'ClientesController@update')->middleware('auth:api');

	});	

});