<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'automator'], function () {

	Route::post('mask', 'AutomatorController@mask');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Route::group(['middleware' => 'auth:web'], function () {


	Route::group(['prefix' => 'automator'], function () {
			
		Route::get('', 'AutomatorController@index');
		Route::post('cols', 'AutomatorController@cols');

	});

	// Pets
	Route::group(['prefix' => 'pets'], function () {
		
		// Tipos
		Route::group(['prefix' => 'tipos'], function () {

			Route::any('', 'PetsTiposController@index');
			Route::any('paginator', 'PetsTiposController2@index');
			Route::get('create', 'PetsTiposController@create');
			Route::post('store', 'PetsTiposController@store');
			Route::get('update/{id}', 'PetsTiposController@update');
			Route::post('save', 'PetsTiposController@save');
			Route::any('destroy/{id?}', 'PetsTiposController@destroy');

		});


		// Raças
		Route::group(['prefix' => 'racas'], function () {

			Route::any('', 'PetsRacasController@index');
			Route::get('create', 'PetsRacasController@create');
			Route::post('store', 'PetsRacasController@store');
			Route::get('update/{id}', 'PetsRacasController@update');
			Route::post('save', 'PetsRacasController@save');
			Route::any('destroy/{id?}', 'PetsRacasController@destroy');

		});

	});


	Route::group(['prefix' => 'servicos'], function () {

		Route::any('', 'ServicosController@index');
		Route::get('create', 'ServicosController@create');
		Route::post('store', 'ServicosController@store');
		Route::get('update/{id}', 'ServicosController@update');
		Route::post('save', 'ServicosController@save');
		Route::any('destroy/{id?}', 'ServicosController@destroy');

	});



	Route::group(['prefix' => 'clientes'], function () {

		// Usuários
		Route::group(['prefix' => 'usuarios'], function () {
			
			Route::any('', 'AppUsersController@index');
			Route::get('create', 'AppUsersController@create');
			Route::post('store', 'AppUsersController@store');
			Route::get('update/{id}', 'AppUsersController@update');
			Route::post('save', 'AppUsersController@save');
			Route::any('destroy/{id?}', 'AppUsersController@destroy');

		});

	});


	// Veterinários
	Route::group(['prefix' => 'veterinarios'], function () {
		
		Route::any('', 'VeterinarioController@index');
		Route::get('create', 'VeterinarioController@create');
		Route::post('store', 'VeterinarioController@store');
		Route::get('update/{id}', 'VeterinarioController@update');
		Route::post('save', 'VeterinarioController@save');
		Route::any('destroy/{id?}', 'VeterinarioController@destroy');

	});

// });